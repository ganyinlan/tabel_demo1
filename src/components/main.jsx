import React from 'react'
import { Layout } from 'antd'
import { Route, BrowserRouter as Router, Link, Switch } from 'react-router-dom'

import MyTable from './MyTable'
import MyHeader from './MyHeader'
import MySearch from './MySearch'
import Home from '../components/Home'
import Page1 from '../components/Page1'
import Page2 from '../components/Page2'


const { Header, Content } = Layout
function App() {
	const keywords = ['姓名', '工号', '序列', '角色', 'EE部门']
	return (
		<>
			<Header>
				<MyHeader />
			</Header>
			<Content>
				<p></p>
				<Router>
					<Switch>
						<Route path="/page1" component={ Page1 } />
						<Route path="/page2" component={ Page2 } />
						<Route path="/" component={ Home } />
					</Switch>
				</Router>
			</Content>
			
		</>
	)
}

export default App
