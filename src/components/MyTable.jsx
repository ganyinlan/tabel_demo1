import React from 'react'
import { Button, Table } from 'antd'
import { data, schemeColumns } from '../db/sourceData'

import 'antd/dist/antd.css'
import '../css/normalization.css'
import '../css/test.css'

function MyTable() {
    return (
        <Table columns={schemeColumns} dataSource={data} scroll={{ x: 2000 }}></Table>
    )
}

export default MyTable; 