import React from 'react'
import { Layout } from 'antd'
import { Route, BrowserRouter as Router, Link } from 'react-router-dom'

import MyTable from './MyTable'
import MySearch from './MySearch'

function Home() {
	const keywords = ['姓名', '工号', '序列', '角色', 'EE部门']
	return (
		<>
            <p></p>
            <MySearch searchWords={keywords} />
            <MyTable />
		</>
	)
}

export default Home
