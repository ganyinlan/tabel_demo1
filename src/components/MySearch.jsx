import React from 'react'
import { Form, Button, Row, Col, Input } from 'antd'

function MySearch(props) {
    const children = []; 
    const { searchWords } = props
    const [form] = Form.useForm();

    const getFields = () => {
        console.log(searchWords)
        for(let i=0, len = searchWords.length; i<len; i++){
            children.push(
                <Col span={8} key={i}>
                    <Form.Item
                        name={searchWords[i]}
                        label={searchWords[i]}
                    >
                        <Input placeholder={searchWords[i]} maxLength={10}/>
                    </Form.Item>
                </Col>, 
            )
        }
        return children
    }

    const onFinish = (values) => {
        console.log('Received values of form: ', values);
    };
    
    return (
        <Form style={{ marginTop: 20, paddingLeft: 50, paddingRight: 50 }}
            form={form}
            name="advanced_search"
            className="ant-advanced-search-form"
            onFinish={onFinish}
        >
            <Row gutter={24}>{getFields()}</Row>
            <Row>
                <Col
                    span={24}
                    style={{
                        textAlign: 'right',
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Search
                    </Button>
                    <Button
                        style={{
                            margin: '0 8px',
                        }}
                        onClick={() => {
                            form.resetFields();
                        }}
                    >
                        Clear
                    </Button>
                </Col>
            </Row>
        </Form>
    )
}

export default MySearch