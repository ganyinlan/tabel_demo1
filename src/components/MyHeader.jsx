import React from 'react'
import { Layout, Menu } from 'antd'

const { Header, Content, Footer } = Layout

function MyHeader() { 
    return (
        <>
            <Header style={ { position: 'fixed', zIndex: 1, width: '100%', height: 64 } }>
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={ ['1'] }>
                    <Menu.Item key="1"> <a href
                    ='/'>首页</a> </Menu.Item>
                    <Menu.Item key="2"> <a href='/page1'>商机</a> </Menu.Item>
                    <Menu.Item key="3">交易</Menu.Item>
                    <Menu.Item key="4">决策</Menu.Item>
                    <Menu.Item key="5">知识</Menu.Item>
                    <Menu.Item key="6">服务</Menu.Item>
                    <Menu.Item key="7">更多应用</Menu.Item>
                </Menu>
            </Header>
        </>
    )
}

export default MyHeader; 