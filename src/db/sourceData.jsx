// 在这里获取数据
// 思路： 根据column 的规则动态创建 表格, 因为列是可选动态的

export const data = [
    {
        name:'甘银兰', 
        id: 12, 
        address: '西溪八方城',
        key: '1',
    },
    {
        name :'小明', 
        id: 32, 
        address: '北京',
        key: '2',
    },
]

const columns = ['姓名', '工号', '在线状态', '直属上级', '雇员类型', '序列', '子序列', 'EE部门', '业务部门', '标准岗位', '角色', '操作']
// const columns = ['姓名', '工号',  '子序列', 'EE部门', '业务部门', '标准岗位', '角色', '操作']
// const columns = ['姓名', '工号', '操作']



function getColumns(columns) {
    // 接受columns，动态生成schemeColumns
    const schemeColumns = []
    columns.forEach( (col, index) => {
        // console.log(index, col)
        const schemeCol = {}
        schemeCol.title = col
        schemeCol.key = col
        switch (col) {
            case '姓名':
                schemeCol.dataIndex = 'name'
                break
            case '操作':
                schemeCol.dataIndex = 'address'
                break
            default :
                schemeCol.dataIndex = 'id'
                break
        }
        if(col === '姓名') {
            schemeCol.fixed = 'left'
            schemeCol.width = 100
        } 
        if(col === '操作') {
            schemeCol.fixed = 'right'
            schemeCol.width = 200
        }
        
        schemeColumns.push(schemeCol)
    } )
    // console.log("schemeColumns: ", schemeColumns)
    return schemeColumns; 
}

export const schemeColumns = getColumns(columns)